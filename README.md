GCMPushNotification
===================

Android push notification using Google Cloud Messaging (GCM) MySql and Php


As per google’s documentation “Google Cloud Messaging for Android (GCM) is a service that helps developers send data from servers to their Android applications on Android devices”. Using this service you can send data to your application whenever new data is available instead of making requests to server in timely fashion. Integrating GCM in your android application enhances user experience and saves lot of battery power.

Overview of Google Cloud Messaging, PHP and MySQL
In this tutorial i used PHP as server side programming language and MySQL as server side database. I installed WAMP server to install php, mysql, apache for me. If you are new to connecting PHP, MySQL to android application, i suggest you go through this tutorial
How to connect Android with PHP, MySQL

You can go through the official documentation http://developer.android.com/google/gcm/index.html
if you want to know more about GCM.

Registering with Google Cloud Messaging
1. Goto Google APIs Console page and create a new project. (If you haven’t created already otherwise it will take you to dashboard)

2. After creating project you can see the project id in the url. Note down the project id which will be used as SENDER ID in android project. (Example: in #project:460866929976 after semicolon 460866929976 is the sender id)

https://code.google.com/apis/console/#project:460866929976




3. After that click on Services on the left panel and turn on Google Cloud Messaging for Android.


4. Once you are done, click on API Access and note down the API Key. This API key will be used when sending requests to GCM server.

Creating MySQL Database
1. Open phpmyadmin panel by going to http://localhost/phpmyadmin and create a database called gcm. (if your localhost is running on port number add port number to url)
2. After creating the database, select the database and execute following query in SQL tab to create gcm_users table.

CREATE TABLE IF NOT EXISTS `gcm_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gcm_regid` text,
  `name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

Creating & Running the PHP Project
When we are making request to GCM server using PHP i used curl to make post request. Before creating php project enable curl module in your php extensions.
Left Click on the WAMP icon the system try -> PHP -> PHP Extensions -> Enable php_curl

1. Goto your WAMP folder and inside www folder create a folder called gcm_server_php. (In my case i installed wamp in C:\WAMP)
You may downlaod the gcm_server_pho from the following link
http://sdu.bz/ZDLISD2k
paste the folder on the www foldr.

Installing helper libraries and setting up the Emulator
Before start writing android code we need to install the helper libraries and make required changes to the emulator.

1. Goto your android SDK folder and open SDK Manager and install Google Cloud Messaging for Android Library under Extras section. (If you don’t see Google Cloud Messaging for Android Library update your SDK manager to latest version or uncheck the obsolete button under the extra section the you could view the Googe Cloud Messaging Library for android install the library )


2. After installing the library it will create gcm.jar file in your Andoird_SDK_Folder\extras\google\gcm\gcm-client\dist. Later you need to add this .jar file to your android project.

3. Now open your AVD Manager and create a new Google API emulator and start the emulator. (Note: To test gcm application in emulator you need to test it on Google API device only)

4. After launching emulator press Menu button goto Settings. Select Accounts & Sync. And then press Add Account button and add a Google account.

Once you are done adding google account you are good to start android project.

Run the project and i’ll be happy to solve the errors if you got any.
